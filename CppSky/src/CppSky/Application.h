#pragma once
#include "Core.h"

namespace CppSky
{

	class CPPSKY_API Application
	{
	public:
		Application();
		virtual ~Application();

		void Run();
	};

	// To be define in  CLIENT
	Application* CreateApplication();
}

