#pragma once

#ifdef CS_PLATFORM_WINDOWS
	#ifdef CS_BUILD_DLL
		#define CPPSKY_API __declspec(dllexport)
	#else
		#define CPPSKY_API __declspec(dllimport)
	#endif
#else
	#error CppSky only supports windows!
#endif