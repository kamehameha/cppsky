#pragma once

#ifdef CS_PLATFORM_WINDOWS

extern CppSky::Application* CppSky::CreateApplication();

int main(int argc, char** argv) {

	printf("CppSky Engine hello!\n");

	auto app = CppSky::CreateApplication();
	app->Run();
	delete app;
}
#endif