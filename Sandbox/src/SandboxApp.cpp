#include "CppSky.h"

class Sandbox : public CppSky::Application
{
public:
	Sandbox() {};
	~Sandbox() {};

};

//void main() {
//	Sandbox* sandbox = new Sandbox();
//	sandbox->Run();
//
//	delete sandbox;
//}

CppSky::Application* CppSky::CreateApplication()
{
	return new Sandbox();
}